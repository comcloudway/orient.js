# Orient.js

![Official logo](./icon.png)

A javascript library for detecting the platform(mobile or desktop) and the orientation(portrait or landscape).

## Usage
Start of by initializing the Orient class
```javascript
import Orient from './index.js';

const config = {
  callback: ()=>{}
  };

let orient = new Orient(config);
```

You can either specify the callback as shown in the listing above or use the `orient.onchange` setter.
```javascript
orient.onchange = ({
  // platform
  is_mobile,
  is_desktop,
  // orientation
  is_portrait,
  is_landscape
  }) => {};
```

To obtain the current state you can use the `orient.current` getter, this will return the same object that is returned in the callbacks
```javascript
{
  // platform
  is_mobile: Boolean,
  is_desktop: Boolean,
  // orientation
  is_portrait: Boolean,
  is_landscape: Boolean
  }
```

To stop Orient.js from tracking the orientation use the `orient.remove()` function.
This will automatically unsupscribe all listeners.

## Credits
- MDN for providing the documentation needed for these old functions
- Safari & Edge for not implementing screen.orientation
- All Phone browsers for still supporting window.orientation
