/**
  * Orientation & Platform detector
  * @constructor @param options - specify options
  */
  class Orient {
    constructor({callback=()=>{}}) {
      this._listeners = [];
      this._callback = callback;

      this._is_mobile = false;
      this._is_portrait = false;

      if (!!window.screen.orientation) {
        // modern browser mobile or desktop
        this._is_mobile = (typeof window.orientation == 'number');
        this._is_portrait = window.screen.orientation.type.includes('portrait');
        let list = screen.orientation.addEventListener('change', e=>{
          let is_portrait = window.screen.orientation.type.includes('portrait');
          if (this._is_portrait != is_portrait) {
            this._is_portrait = is_portrait;
            this._callback(this.current);
          }
        });
        this._listeners.push(()=>screen.orientation.removeEventListener('change', list));
      } else if (typeof window.orientation == 'number') {
        // smartphone browser
        this._is_mobile = true;
        this._is_portrait = !(Math.abs(window.orientation) == 90);
        let list = window.addEventListener('deviceorientation', e=>{
          let is_portrait = !(Math.abs(window.orientation) == 90);
          if (this._is_portrait != is_portrait) {
            this._is_portrait = is_portrait;
            this._callback(this.current);
          }
        });
        this._lsteners.push(()=>window.removeEventListener('deviceorientation', list));
      } else {
        // outdated browser
        this._is_mobile = false;
        this._is_portrait = window.screen.height > window.screen.width;
        let list = window.addEventListener('resize', ()=>{
          // most likely portrait mode
          let is_portrait = window.screen.height > window.screen.width;
          if (this._is_portrait != is_portrait) {
            this._is_portrait = is_portrait;
            this._callback(this.current);
          }
        });
        this._listeners.push(()=>window.removeEventListener('resize', list));
      }
    }

    /**
      * Returns the current state
      * @getter current
      */
      get current() {
        return {
          portrait: this._is_portrait,
          landscape: !this._is_portrait,
          mobile: this._is_mobile,
          desktop: !this._is_mobile
        }
      }

    /**
      * Sets the onchange listener
      * @setter onchange
      */
      set onchange(callback=()=>{}) {
        this._callback=callback;
      }

    /** Unsubscribes to running event listeners
      * @function remove
      */
      remove() {
        this._listeners.forEach(cb => {
          cb.forEach(c=>c());
        })
      }
  };

export default Orient;
